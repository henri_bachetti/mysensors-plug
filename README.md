# MYSENSORS CONNECTED PLUG

The purpose of this page is to explain step by step the realization of a connected plug based on ARDUINO PRO MINI, connected by radio to a DOMOTICZ server, using an NRF24L01 2.4GHZ module.

The board uses the following components :

 * an ARDUINO PRO MINI 3.3V 8MHz
 * a NRF24L01
 * a MEANWELL IRM-03-5 power supply
 * a MCP1702 3.3V regulator
 * an OMRON-G5RL-K1A-E-5DC relay
 * 2 2N3904 transistors
 * some passive components

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2018/02/une-prise-connectee-mysensors.html
