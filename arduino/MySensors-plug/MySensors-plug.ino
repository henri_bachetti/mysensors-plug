/**
 * The MySensors Arduino library handles the wireless radio link and protocol
 * between your home built sensors/actuators and HA controller of choice.
 * The sensors forms a self healing radio network with optional repeaters. Each
 * repeater and gateway builds a routing tables in EEPROM which keeps track of the
 * network topology allowing messages to be routed to nodes.
 *
 * Created by Henrik Ekblad <henrik.ekblad@mysensors.org>
 * Copyright (C) 2013-2015 Sensnology AB
 * Full contributor list: https://github.com/mysensors/Arduino/graphs/contributors
 *
 * Documentation: http://www.mysensors.org
 * Support Forum: http://forum.mysensors.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 *******************************
 *
 * REVISION HISTORY
 * Version 1.0 - Henrik Ekblad
 *
 * DESCRIPTION
 * Example sketch showing how to control physical relays.
 * This example will remember relay state after power failure.
 * http://www.mysensors.org/build/relay
 */

// Enable debug prints to serial monitor
//#define MY_DEBUG

// Enable and select radio type attached
#define MY_RADIO_NRF24
//#define MY_RADIO_RFM69

#define MY_RF24_CE_PIN 7
#define MY_RF24_CS_PIN 8
#define CHILD_ID 0

// Enable repeater functionality for this node
#define MY_REPEATER_FEATURE

#include <MySensors.h>

#define RELAY_RESET   4
#define RELAY_SET     3
#define GREEN_LED     5
#define RED_LED       6
#define MANUAL_SWITCH 9
#define RELAY_ON      1  // GPIO value to write to turn on attached relay
#define RELAY_OFF     0 // GPIO value to write to turn off attached relay

int buttonState;             // the current reading from the input pin
int lastButtonState = LOW;   // the previous reading from the input pin

long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 50;    // the debounce time; increase if the output flickers
MyMessage msg(CHILD_ID, V_LIGHT);

void relayOn(void)
{
  Serial.println("Relay ON");
  digitalWrite(RELAY_SET, RELAY_ON);
  delay(50);
  digitalWrite(RELAY_SET, RELAY_OFF);
  digitalWrite(RED_LED, 0);
}

void relayOff(void)
{
  Serial.println("Relay OFF");
  digitalWrite(RELAY_RESET, RELAY_ON);
  delay(50);
  digitalWrite(RELAY_RESET, RELAY_OFF);
  digitalWrite(RED_LED, 1);
}

void before()
{
  Serial.print("MYSENSORS plug: ");
	// Then set relay pins in output mode
  pinMode(RELAY_RESET, OUTPUT);
  pinMode(RELAY_SET, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
  pinMode(RED_LED, OUTPUT);
  pinMode(MANUAL_SWITCH, INPUT_PULLUP);
  digitalWrite(GREEN_LED, 1);
  digitalWrite(RED_LED, 1);
	// Set relay to last known state (using eeprom storage)
  Serial.println("OK");
}

void setup()
{
}

void presentation()
{
  Serial.print("Presentation: ");
	// Send the sketch version information to the gateway and Controller
	sendSketchInfo("MYSENSORS plug", "1.0");
  present(CHILD_ID, S_BINARY);
  Serial.println("OK");
  digitalWrite(GREEN_LED, 0);
}


void loop()
{
  static bool restored = false;

  if (!restored) {
    if (loadState(CHILD_ID) == 1) {
      relayOn();
    }
    else {
      relayOff();
    }
    restored  = true;
  }
  int state = digitalRead(MANUAL_SWITCH);
  if (state != lastButtonState) {
    lastDebounceTime = millis();
  }
  if ((millis() - lastDebounceTime) > debounceDelay) {
    if (state != buttonState) {
      buttonState = state;
      if (buttonState == LOW) {
        if (loadState(CHILD_ID) == 0) {
          relayOn();
          saveState(CHILD_ID, 1);
          send(msg.set(1), 1);
        }
        else {
          relayOff();
          saveState(CHILD_ID, 0);
          send(msg.set(0), 1);
        }
      }
    }
  }
  lastButtonState = state;
}

void receive(const MyMessage &message)
{
	// We only expect one type of message from controller. But we better check anyway.
  if (message.isAck()) {
     Serial.println("ACK from gateway");
  }
  else {
  	if (message.type==V_STATUS) {
  		// Change relay state
  		if (message.getBool() == 1) {
        relayOn();
  		}
      else {
        relayOff();
      }
  		// Store state in eeprom
  		saveState(message.sensor, message.getBool());
  		// Write some debug info
  		Serial.print("Incoming change for sensor:");
  		Serial.print(message.sensor);
  		Serial.print(", New status: ");
  		Serial.println(message.getBool());
  	}
  }
}

