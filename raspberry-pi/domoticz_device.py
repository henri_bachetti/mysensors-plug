
import json
import urllib2
import re
import time
import datetime
import httplib, urllib
 
def open_port():
	pass
 
def close_port():
	pass
 
 
class DomoticzDevice():
 
	def __init__(self, url):
		self.baseurl = url

	def __execute__(self, url):
		req = urllib2.Request(url)
		return urllib2.urlopen(req, timeout=5)

	def getDeviceId(self, name):
		"""
		Get the Domoticz device id
		"""
		url = u"%s/json.htm?type=devices&filter=all&used=true&order=Name&" % (self.baseurl)
		print url
		data = json.load(self.__execute__(url))
		for device in data['result']:
			if device['Name'] == name:
				return device['idx']
		return None

	def getDeviceData(self, name):
		"""
		Get the Domoticz device data
		"""
		url = u"%s/json.htm?type=devices&filter=all&used=true&order=Name&" % (self.baseurl)
		print url
		data = json.load(self.__execute__(url))
		for device in data['result']:
			if device['Name'] == name:
				return device['Data']
		return None

	def setDeviceInfo(self, xid, category, info):
		"""
		Set the Domoticz device information.
		"""
		url = "%s/json.htm?type=command&param=%s&idx=%s&switchcmd=%s&" % (self.baseurl, category, xid, info)
		print url
		data = json.load(self.__execute__(url))
		return data

