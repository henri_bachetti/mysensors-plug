EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:front-panel-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Led_Small D2
U 1 1 5984AD47
P 6350 3400
F 0 "D2" H 6400 3350 50  0000 L CNN
F 1 "GREEN_LED" H 5700 3400 50  0000 L CNN
F 2 "LEDs:LED-3MM" V 6350 3400 50  0001 C CNN
F 3 "" V 6350 3400 50  0000 C CNN
	1    6350 3400
	-1   0    0    1   
$EndComp
$Comp
L Led_Small D1
U 1 1 5984AD7D
P 6350 3300
F 0 "D1" H 6400 3250 50  0000 L CNN
F 1 "RED_LED" H 5800 3300 50  0000 L CNN
F 2 "LEDs:LED-3MM" V 6350 3300 50  0001 C CNN
F 3 "" V 6350 3300 50  0000 C CNN
	1    6350 3300
	-1   0    0    1   
$EndComp
$Comp
L SW_PUSH SW1
U 1 1 5984AF3E
P 5750 3700
F 0 "SW1" H 5900 3810 50  0000 C CNN
F 1 "MANUAL" H 5750 3950 50  0000 C CNN
F 2 "library:µSW" H 5750 3700 50  0001 C CNN
F 3 "" H 5750 3700 50  0000 C CNN
	1    5750 3700
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X05 P1
U 1 1 5984AF8A
P 5150 3600
F 0 "P1" H 5150 3950 50  0000 C CNN
F 1 "WIFI-RELAY" V 5250 3600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x05" H 5150 3600 50  0001 C CNN
F 3 "" H 5150 3600 50  0000 C CNN
	1    5150 3600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5350 3700 5450 3700
Wire Wire Line
	6050 3700 6150 3700
Wire Wire Line
	6150 3700 6150 3900
Wire Wire Line
	5450 3900 5450 3800
Wire Wire Line
	5450 3800 5350 3800
$Comp
L R R1
U 1 1 59851C91
P 5900 3300
F 0 "R1" V 5850 3450 50  0000 C CNN
F 1 "3.3K" V 5900 3300 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5830 3300 50  0001 C CNN
F 3 "" H 5900 3300 50  0000 C CNN
	1    5900 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	6050 3300 6250 3300
$Comp
L R R2
U 1 1 59851D7E
P 5900 3400
F 0 "R2" V 5850 3550 50  0000 C CNN
F 1 "3.3K" V 5900 3400 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5830 3400 50  0001 C CNN
F 3 "" H 5900 3400 50  0000 C CNN
	1    5900 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	6050 3400 6250 3400
Wire Wire Line
	6450 3300 6550 3300
Wire Wire Line
	6550 3600 5350 3600
$Comp
L +3.3V #PWR01
U 1 1 59851F24
P 5600 3200
F 0 "#PWR01" H 5600 3050 50  0001 C CNN
F 1 "+3.3V" H 5600 3340 50  0000 C CNN
F 2 "" H 5600 3200 50  0000 C CNN
F 3 "" H 5600 3200 50  0000 C CNN
	1    5600 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3200 5600 3400
Wire Wire Line
	5350 3400 5750 3400
Wire Wire Line
	5600 3300 5750 3300
Connection ~ 5600 3300
Connection ~ 5600 3400
Wire Wire Line
	6550 3300 6550 3600
Wire Wire Line
	6450 3400 6450 3500
Wire Wire Line
	6450 3500 5350 3500
$Comp
L GND #PWR?
U 1 1 5985403B
P 5400 4000
F 0 "#PWR?" H 5400 3750 50  0001 C CNN
F 1 "GND" H 5400 3850 50  0000 C CNN
F 2 "" H 5400 4000 50  0000 C CNN
F 3 "" H 5400 4000 50  0000 C CNN
	1    5400 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 4000 5400 3700
Connection ~ 5400 3700
Wire Wire Line
	6150 3900 5450 3900
$EndSCHEMATC
